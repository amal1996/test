# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table post (
  id                            serial not null,
  title                         varchar(255),
  body                          varchar(255),
  id_startup                    varchar(255),
  created_at                    timestamp default now(),
  constraint pk_post primary key (id)
);


# --- !Downs

drop table if exists post cascade;

